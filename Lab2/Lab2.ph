﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

******************************************************************************/

#requires @"Lab2\Lab2.pack"
% publicly used packages
#include @"pfc\console\console.ph"
#include @"pfc\core.ph"

% exported interfaces

% exported classes
#include @"Lab2\Lab22.cl"
#include @"Lab2\Lab2.cl"
