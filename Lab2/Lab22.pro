﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

                        Task: Вариант 6

******************************************************************************/

implement lab22
    open core, console

    domains
        point = point(real X, real Y).
        line = line(point Begin, point End).

    class facts
        area: (line, line, line).

    class predicates
        pointInArea: (point, boolean [out]) determ.
        askSign: (point, point, point, real [out]).
        oneSign: (point, point, point, point, boolean [out]) multi.

    clauses

        area(line(point(-0.5,-0.5),point(1,1)), line(point(1,1),point(1,-0.5)), line(point(1,-0.5), point(-0.5,-0.5))).

        pointInArea(P, Result):-
            area(line(P1, P2), line(P2, P3), line(P3, P1)), oneSign(P, P1, P2, P3, IsOneSign), Result = IsOneSign, !.

        askSign(point(X,Y), point(X1,Y1), point(X2,Y2), Number):- Number=(X-X2)*(Y1-Y2)-(X1-X2)*(Y-Y2).

        oneSign(P, P1, P2, P3, IsOneSign):-
            askSign(P, P1, P2, Number1), askSign(P, P2, P3, Number2), askSign(P, P3, P1, Number3),
                (((Number1 <= 0), (Number2 <= 0), (Number3 <= 0));
                ((Number1 >= 0), (Number2 >= 0), (Number3 >= 0))),
            IsOneSign = true; IsOneSign = false.

        run():- pointInArea(point(0,0), IsInside), write("is point in area: ", IsInside), fail;
        _ = readLine().


end implement lab22