﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

                        Task: №20. Числа а и b выражают длины  катетов одного прямоугольного треугольника, с и d - другого. Определить являются ли треугольники подобными
                                Тест1 (a=10,b=6,c=5,d=3 ответ подобны)¶Тест2 (a=10,b=7,c=5,d=3 ответ  не подобны)


******************************************************************************/

implement lab2
    open core, console

    class predicates
        isSimilarTriangles: (integer ПервыйКатетПервогоТреугольника, integer ВторойКатетПервогоТреугольника, integer ПервыйКатетВторогоТреугольника, integer ВторойКатетВторогоТреугольника, boolean Результат [out]).


    clauses
        isSimilarTriangles(A, B, C, D, Result):- R1 = A*D, R2 = B*C, if R1=R2 then Result=true else Result=false end if.

        run():-
        write("T11= "), T11 = read(), clearInput(),
        write("T12= "), T12 = read(), clearInput(),
        write("T21= "), T21 = read(), clearInput(),
        write("T22= "), T22 = read(), clearInput(),
        isSimilarTriangles(T11, T12, T21, T22, Result), write("result: ", Result), fail;
        _ = readLine().


end implement lab2