﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

******************************************************************************/

#requires @"Lab6\Lab6.pack"
% publicly used packages
#include @"pfc\console\console.ph"
#include @"pfc\core.ph"

% exported interfaces

% exported classes
#include @"Lab6\Lab6.cl"
