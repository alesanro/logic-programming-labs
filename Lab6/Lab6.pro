﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

                        Task: 5. Создайте программу, позволяющую диагностировать неисправность телевизора. ЭС должна быть основана на логике.

******************************************************************************/

implement lab6
    open core, console

    class facts - db
    	issue: (positive IssueNumber, string Topic, string IssueName, positive* States).
    	element_state: (positive StateNumber, string Target, string Value).
    	topic: (string Name).

    class facts
    	answer: (positive, integer).

    class predicates
    	expert: (string TopicName).
    	ask: (positive IssueNumber, positive* AvaibleHypothesisStates) determ.
    	ask: (integer UserInteractionAnswer, positive IssueNumber, positive* AvaibleHypothesisStates) determ.
    	explanation: (positive IssueNumber).
    	getAnswer: () -> integer. % Get user interaction input answer
    	getAnswer: (char UserInput) -> integer determ. % Convert user input to answer
    	correct: (positive* CurrentElementStates) determ.

    clauses
    	expert(Topic):-
    		issue(K, Topic, IssueName, StatesList),
    		ask(K, StatesList),
    		correct(StatesList),
    		!,
    		writef("Заключение: % - %.", Topic, IssueName).
    	expert(Topic):-
    		writef("Не хватает сведений, чтобы определить %.", Topic).


    	ask(_, [N|_]):-
    		answer(N,0),
    		!,
    		fail.
    	ask(K, [N|StatesList]):-
    		answer(N, 1),
    		!,
    		ask(K, StatesList).
    	ask(K, [N|StatesList]):-
    		element_state(N, X, Y),
    		writef("Верно ли, что % - %? (да/нет/?) :> ", X, Y),
    		A = getAnswer(),
    		!,
    		ask(A, K, [N|StatesList]).
    	ask(_, []).

    	ask(2, K, StatesList):-!,
    		explanation(K),
    		ask(K, StatesList).
    	ask(A, _, [N|_]):-
    		assert(answer(N, A)),
    		fail.
    	ask(1, K, [_|StatesList]):-
    		ask(K, StatesList).

    	explanation(K):-
    		issue(K, Topic, IssueName, StatesList),
    		writef("Проверяется гипотеза, что % - %. Это так, если\n", Topic, IssueName),
    		F = {(I):- element_state(I, X, Y), !, writef("\t% - %\n", X, Y); succeed},
    		list::forAll(StatesList, F),
    		L = [N || answer(N,1), list::isMember(N, StatesList)],
    		L <> [],
    		!,
    		write("Известно, что:\n"),
    		list::forAll(L, F).
    	explanation(_).

    	correct(StatesList):-
    		answer(N, 1),
    		not(list::isMember(N, StatesList)),
    		!,
    		fail;
    		succeed().

    	getAnswer() = R:-
    		S = string::frontChar(string::toLowerCase(string::trim(readLine()))),
    		R = getAnswer(S),
    		!.
    	getAnswer() = 0.
    	getAnswer(S) = 1:-
    		(S = 'д'; S = 'y'; S = '1'),
    		!.
    	getAnswer('?') = 2.

        run():-
        	file::consult("expert_system.txt", db),
        	topic(Topic),
        	expert(Topic),
            fail;
            _ = readLine().


end implement lab6