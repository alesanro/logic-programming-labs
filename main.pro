﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

******************************************************************************/

implement main
    open core

clauses
    run():-
        console::init(),
        succeed(). % place your own code here
end implement main

goal
    mainExe::run(main::run),
    %lab2::run().
    %lab22::run().
    %lab3::run().
    %lab4::run().
    %lab42::run().
    %lab5::run().
    lab6::run().