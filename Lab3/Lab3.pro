﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

                        Task: Вариант 8. Дана последовательность a с элементами из множества {0,1}. Проводятся следующие действия.
                                Если a имеет вид 1,0,1,… , то она укорачивается на первые три элемента. В противном случае начальный элемент
                                последовательности переносится в её конец. Указанные действия повторяются до тех пор, пока имеется возможность
                                укоротить текущую последовательность. Требуется составить рекурсивную программу, имитирующую эти действия
                                и возвращающую по исходной последовательности a результирующую последовательность b или сообщение,
                                что b - пустое множество.

******************************************************************************/

implement lab3
    open core, console

    domains
        list_int = integer*.

    class predicates
        slice: (list_int Последовательность, integer ТекущийСчетчик, boolean СледуетПродолжить, list_int ВыходнаяПоследовательность [out]) determ.
        length: (list_int, integer [out]).
        append: (integer, list_int, list_int [out]).


    clauses
        append(E, [], [E]):-!.
        append(E, [H|Tail], [H|Tail1]):-
            append(E, Tail, Tail1).

        length([],0).
        length([_|T],L):-
            length(T,L1), L=L1+1.

        slice([], _, false, []):-!.
        slice(A, _, false, A):-!.
        slice([1,0,1 | T], _, ShoudContinue, OutputList):-
            Counter = 0,
            slice(T, Counter, ShoudContinue, OutputList), !.

        slice([A|T], Counter, _, Tail2):-
            length(T, LengthOfT),
            append(A, T, Tail1),
            if LengthOfT > Counter then
                ShouldContinue = true
            else
                ShouldContinue = false
            end if,
            Counter1 = Counter + 1,
            slice(Tail1, Counter1, ShouldContinue, Tail2), !.


        run():-
        slice([1,0,1,0,0,1,0,1,1,0,0,1,0,1], 0, true ,Seq),
        length(Seq, Length),
        if Length > 0 then
            write("Result sequence: ", Seq)
        else
            write("The result sequence in empty!")
        end if,
        fail;
        _ = readLine().

%        run():-append(4, [1,2,3], Result), write("Result array: ", Result), fail;
%        _ = readLine().


end implement lab3