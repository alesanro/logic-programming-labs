﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

******************************************************************************/

#requires @"Lab3\Lab3.pack"
% publicly used packages
#include @"pfc\console\console.ph"
#include @"pfc\core.ph"

% exported interfaces

% exported classes
#include @"Lab3\Lab3.cl"
