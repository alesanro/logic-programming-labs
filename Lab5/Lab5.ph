﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

******************************************************************************/

#requires @"Lab5\Lab5.pack"
% publicly used packages
#include @"pfc\console\console.ph"
#include @"pfc\core.ph"

% exported interfaces

% exported classes
#include @"Lab5\Lab5.cl"
