﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

                        Task: 1. Создайте предикат, который подсчитает общее количество латинских букв в наборе символов.
                                    Используя Пролог, напишите программу для работы со строками и с файлом (чтение/запись)

******************************************************************************/

implement lab5
    open core, console

    class facts - test_lab5
        phone: (string Name, string PhoneNumber).

    class predicates
        load: (string Filename) determ.
        save: (string Filename).
        printDb: ().
        add: (string Name, string Phone).
        delete: (string Name).
        delete: (string Name, string Phone).
        defineLatinSet: (char Symbol, boolean IsLatin [out]) determ.
        defineLatinSet: (char Symbol, char* AvaibleLatinCharacters, boolean IsLatin [out]).
        defineLatins: (string String, integer NumberOfChars [out]) determ.
        defineLatins1: (char* String, integer NumberOfChars [out]) determ.
        stringList: (string InputString, char* OutputList [out]) determ.


    clauses
        stringList("", []):-!.
        stringList(String, [Char|Tail]):-
            string::frontChar(String, Char, String1),
            stringList(String1, Tail).

        defineLatinSet(Symbol, IsLatin):-
            stringList("abcdefghijklmnopqrstuvwxyzABCDIFGHIJKLMNOPQRSTUVWXYZ", Chars),
            defineLatinSet(Symbol, Chars, IsLatin).

        defineLatinSet(_, [], false):-!.
        defineLatinSet(Symbol, [S|T], IsLatin):-
            if Symbol = S then
                IsLatin = true
            else
                defineLatinSet(Symbol, T, IsLatin)
            end if.

        defineLatins1([], 0):-!.
        defineLatins1([H|T], Number):-
            defineLatinSet(H, IsLatin),
            defineLatins1(T, Number1),
            if IsLatin = true then
                Number = Number1 + 1
            else
                Number = Number1 + 0
            end if.

        defineLatins(S, Number):-
            stringList(S, InputChars),
            defineLatins1(InputChars, Number).

        load(Filename):-
            file::existFile(Filename),
            !,
            file::consult(Filename, test_lab5)	,
            succeed().

        save(Filename):-
            file::save(Filename, test_lab5).

        printDb():-
            phone(Name, Phone),
                writef("  %-20%\n", Name, Phone),
            fail;
            succeed().

        add(Name, Phone):-
            phone(Name, Phone),
            !,
            writef("Запись % - % уже есть в базе данных\n",
                Name, Phone);
            assert(phone(Name, Phone)),
            writef("Запись '% - %' добавлена\n", Name, Phone).

        delete(Name):-
            not(phone(Name, _)),
            !,
            writef("Записей для '%' нет\n", Name);
            retractall(phone(Name, _)),
            writef("Записи для '%' удалены\n", Name).

        delete(Name, Phone):-
            retract(phone(Name, Phone)),
            !,
            writef("Запись '% - %' удалена\n", Name, Phone);
            writef("Записи '% - %' нет в базе данных\n", Name, Phone).


        run():-
            Filename = "test_lab5.txt",
            load(Filename),
            printDb(),
            add("Sarah", "+9 23 44 81 46"),
            add("Mike", "+8 23 92 88 00"),
            add("Mike", "+8 23 92 00 11"),
            add("Mike", "+8 23 92 66 00"),
            delete("Mike", "+8 23 92 00 11"),
            delete("Mike"),
            save(Filename),

            defineLatins("my имя есть Alex!", Chars),
            write("chars is ", Chars),
            fail;
            _ = readLine().


end implement lab5