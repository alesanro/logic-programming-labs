﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

******************************************************************************/

#requires @"Lab4\Lab4.pack"
% publicly used packages
#include @"pfc\console\console.ph"
#include @"pfc\core.ph"

% exported interfaces

% exported classes
#include @"Lab4\Lab42.cl"
#include @"Lab4\Lab4.cl"
