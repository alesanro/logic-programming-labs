﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

                        Task:  18. Создайте предикат, удаляющий из исходного списка элементы с четными номерами.

******************************************************************************/

implement lab42
    open core, console

    domains
        list = integer*.

    class predicates
        removeEvenElements: (list, list [out]) determ.
        removeEvenElements: (list, integer, list [out]) determ.

    clauses
        removeEvenElements([], []):-!.
        removeEvenElements(L1, L2):-
            removeEvenElements(L1, 1, L2), !.

        removeEvenElements([], _, []):-!.
        removeEvenElements([_|T], Index, T1):-
            Index mod 2 = 0,
            !,
            Index1 = Index + 1,
            removeEvenElements(T, Index1, T1).
        removeEvenElements([H|T], Index, [H|T1]):-
            Index1 = Index + 1,
            removeEvenElements(T, Index1, T1).


        run():-
        removeEvenElements([1,2,3,4,5,6,7], Removed),
        write(Removed),
        removeEvenElements([], Removed1),
        write(Removed1),
        fail;
        _ = readLine().


end implement lab42