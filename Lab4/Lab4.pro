﻿/*****************************************************************************

                        Copyright (c) 2014 My Company

                        Task: 32. Создайте предикат, осуществляющий циклический сдвиг элементов списка на заданное количество шагов влево (вправо).

******************************************************************************/

implement lab4
    open core, console

    domains
        list = elements*.
        %elements = i(integer); r(real).
        elements = integer.
        direction = direction(symbol).



    class predicates
        shiftPositions: (direction, integer, integer, integer [out]) determ.
        shift: (list, direction, integer, list [out]).
        shift: (list, integer, list [out]) nondeterm.
        concat: (list, list, list [out]).
        length: (list, integer [out]).

    clauses
        shiftPositions(direction("left"), L, P, R):-
            P > L,
            R = P mod L,  !;
            R = P.

        shiftPositions(direction("right"), L, P, R):-
            P > L,
            Normalized = P mod L, R = L - Normalized, !;
            R = L - P.

        concat([],L2, L2):-!.
        concat([H|L1], L2, [H|L3]):-
            concat(L1, L2, L3).

        length([], 0):-!.
        length([_|T], L):-
            length(T, L1),
            L = L1 + 1.


        shift(L, _, 0, L):-!.
        shift(A, Dir, Positions, Result):-
            length(A, Length),
            Length > 0,
            shiftPositions(Dir, Length, Positions, Pos),
            shift(A, Pos, Result),
            !;
            Result = [].


        shift(A, 0, A):-!.
                shift([], _, []):-
            write("Sequence is empty"),!.
        shift([H|T], Pos, Res):-
            concat(T, [H], Modified),
            Pos1 = Pos - 1,
            shift(Modified, Pos1, Res).

        run():-
        shift([1,2,3,4,5,6,7,8], direction("left"), 4, R4),
        write("   ls = ", R4),
        shift([1,2,3,4,5,6,7,8], direction("left"), 10, R5),
        write("   ls = ", R5),
        shift([1,2,3,4,5,6,7,8], direction("right"), 2, R6),
        write("   rs = ", R6),
        shift([1,2,3,4,5,6,7,8], direction("right"), 13, R7),
        write("   rs = ", R7),
        shift([], direction("right"), 13, R8),
        write("   rs = ", R8),
        fail;
        _ = readLine().


end implement lab4